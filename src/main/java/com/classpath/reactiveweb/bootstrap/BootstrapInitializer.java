package com.classpath.reactiveweb.bootstrap;

import com.classpath.reactiveweb.model.Movie;
import com.classpath.reactiveweb.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.UUID;

@Component
@AllArgsConstructor
@Slf4j
public class BootstrapInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private MovieRepository movieRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        this.movieRepository
                .deleteAll()
                .thenMany(
                        Flux.just("A", "B", "C", "C")
                        .map(name -> new Movie(UUID.randomUUID().toString(), name, LocalDate.now()))
                        .flatMap((movie) -> this.movieRepository.save(movie))
                )
                .thenMany(movieRepository.findAll())
                .subscribe(movie -> log.info("saving :: "+movie.toString()));
    }
}