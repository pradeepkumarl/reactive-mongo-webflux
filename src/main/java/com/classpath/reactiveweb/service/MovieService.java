package com.classpath.reactiveweb.service;
import com.classpath.reactiveweb.model.Movie;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {

    Flux<Movie> all() ;

    Mono<Movie> get(String id) ;

    Mono<Movie> update(String id, String name);

    Mono<Movie> delete(String id);

    Mono<Movie> create(String name);
}