package com.classpath.reactiveweb.service;

import com.classpath.reactiveweb.model.Movie;
import org.springframework.context.ApplicationEvent;

public class MovieCreatedEvent extends ApplicationEvent {
    public MovieCreatedEvent(Movie movie) {
        super(movie);
    }
}