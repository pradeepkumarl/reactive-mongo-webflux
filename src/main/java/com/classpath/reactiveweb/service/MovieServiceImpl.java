package com.classpath.reactiveweb.service;

import com.classpath.reactiveweb.model.Movie;
import com.classpath.reactiveweb.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final ApplicationEventPublisher publisher;
    private final MovieRepository movieRepository;

    @Override
    public Flux<Movie> all() {
        return this.movieRepository.findAll();
    }

    @Override
    public Mono<Movie> get(String id) {
        return this.movieRepository.findById(id);
    }

    @Override
    public Mono<Movie> update(String id, String name) {
        return this.movieRepository.findById(id)
                .map(movie -> new Movie(movie.getId(), name, LocalDate.now()))
                .flatMap(this.movieRepository::save);
    }

    @Override
    public Mono<Movie> delete(String id) {
        return this.movieRepository.findById(id)
                .flatMap(movie -> this.movieRepository.deleteById(movie.getId()).thenReturn(movie));
    }

    @Override
    public Mono<Movie> create(String name) {
        return this.movieRepository
                .save(new Movie(UUID.randomUUID().toString(), name, LocalDate.now()))
                .doOnSuccess(movie -> this.publisher.publishEvent(new MovieCreatedEvent(movie)));
    }
}