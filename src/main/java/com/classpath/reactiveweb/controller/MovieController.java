package com.classpath.reactiveweb.controller;

import com.classpath.reactiveweb.model.Movie;
import com.classpath.reactiveweb.repository.MovieRepository;
import com.classpath.reactiveweb.service.MovieService;
import lombok.AllArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.net.URI;

@RestController
@RequestMapping(value = "/api/v1/movies", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class MovieController {

    private final MediaType mediaType = MediaType.APPLICATION_JSON;
    private MovieService movieService;

    @GetMapping
    Publisher<Movie> getAll() {
        return this.movieService.all();
    }


    @GetMapping("/{id}")
    Publisher<Movie> getById(@PathVariable("id") String id) {
        return this.movieService.get(id);
    }


    @PostMapping
    Publisher<ResponseEntity<Movie>> create(@RequestBody Movie movie) {
        return this.movieService
                .create(movie.getName())
                .map(p -> ResponseEntity.created(URI.create("/movies/" + p.getId()))
                        .contentType(mediaType)
                        .build());
    }

    @DeleteMapping("/{id}")
    Publisher<Movie> deleteById(@PathVariable String id) {
        return this.movieService.delete(id);
    }

    @PutMapping("/{id}")
    Publisher<ResponseEntity<Movie>> updateById(@PathVariable String id, @RequestBody Movie movie) {
        return Mono
                .just(movie)
                .flatMap(p -> this.movieService.update(id, p.getName()))
                .map(p -> ResponseEntity
                        .ok()
                        .contentType(this.mediaType)
                        .build());
    }
}